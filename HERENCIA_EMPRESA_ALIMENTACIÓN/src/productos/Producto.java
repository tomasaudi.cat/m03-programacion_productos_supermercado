package productos;

import java.util.Date;

public class Producto {
	private Date fechaCaducidad;
	private String numeroLote;
	
	//CONSTRUCTOR
	public Producto(Date fechaCaducidad, String numeroLote) {
		this.fechaCaducidad = fechaCaducidad;
		this.numeroLote = numeroLote;
	}
	
	//SETTERS Y GETTERS
	public Date getfechaCaducidad() {
		return fechaCaducidad;
	}

	public void setfechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}
	
	public String getNumeroLote() {
		return numeroLote;
	}
	
	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}
	
	 @Override
	    public String toString() {
	        return "Fecha de caducidad: " + fechaCaducidad + ", Número de lote: " + numeroLote;
	    }
}
