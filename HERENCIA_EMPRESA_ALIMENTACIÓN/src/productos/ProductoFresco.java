package productos;

import java.util.Date;

	//CLASE PRODUCTOS FRESCOS HEREDADNDO DATOS DE PRODUCTO
public class ProductoFresco extends Producto {
	private Date fechaEnvasado;
	private String paisOrigen;
	
	//CONSTRUCTOR DE LA CLASE
	public ProductoFresco (Date fechaCaducidad, String numeroLote, Date fechaEnvasado,String paisOrigen) {
		super(fechaCaducidad, numeroLote);
		this.fechaEnvasado = fechaEnvasado;
		this.paisOrigen = paisOrigen;
	}
	
	//GETTERS Y SETTERS
	public Date setfechaEnvasado(){
		return fechaEnvasado;
		}
	
	public void getfechaEnvasado(Date fechaEnvasado) {
		this.fechaEnvasado = fechaEnvasado;
	}
	
	public String setpaisOrigen() {
		return paisOrigen;
	}
	
	public void getpaisOrigen(String paisOrigen) {
		this.paisOrigen = paisOrigen;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Fecha envasado: " +fechaEnvasado +  " País de origen: " +paisOrigen;
	}
}



