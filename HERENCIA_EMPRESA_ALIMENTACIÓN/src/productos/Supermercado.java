package productos;

import java.util.Date;

public class Supermercado {
	public static void main(String[] args) {
	
	ProductoFresco ProductoFresco = new ProductoFresco(new Date(), "4987CC", new Date(), "España");
    ProductoRefrigerado ProductoRefrigerado = new ProductoRefrigerado(new Date(), "456FG", "4485CC");
    ProductoCongelado ProductoCongelado = new ProductoCongelado(new Date(), "789ER", -16.3);	
	
	System.out.println("Producto fresco: ");
	System.out.println(ProductoFresco);
	
	System.out.println("\nProducto Refrigerado: ");
	System.out.println(ProductoRefrigerado);
	
	System.out.println("\nProducto congelado: ");
	System.out.println(ProductoCongelado);
	
	}
}
