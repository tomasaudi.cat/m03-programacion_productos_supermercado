package productos;

import java.util.Date;

public class ProductoRefrigerado extends Producto {

	private String codigoOrganismo;
	
	//CONSTRUCTOR
	public ProductoRefrigerado(Date fechaCaducidad, String numeroLote, String codigoOrganismo) {
		super(fechaCaducidad, numeroLote);
		this.codigoOrganismo = codigoOrganismo;
	}

	//GETTERS Y SETTERS

	public String setcodigoOrganismo(){
		return codigoOrganismo;
	}
	
	public void getcodigoOrganismo(String codigoOrganismo) {
		this.codigoOrganismo = codigoOrganismo;
	}

	 @Override
	    public String toString() {
	        return "Código del organismo de supervisión alimentaria:  " + codigoOrganismo;
	    }

	}


