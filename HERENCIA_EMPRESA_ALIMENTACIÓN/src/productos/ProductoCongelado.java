package productos;

import java.util.Date;

public class ProductoCongelado extends Producto {

	private double temperaturaCongelacion;

	//CONSTRUCTOR
	public ProductoCongelado(Date fechaCaducidad, String numeroLote, double temperaturaCongelacion) {
		super(fechaCaducidad, numeroLote);
		this.temperaturaCongelacion = temperaturaCongelacion;
	}

	//GETTERS Y SETTERS

	public double settemperaturaCongelacion() {
		return temperaturaCongelacion;
	}
	
	public void gettemperaturaCongelacion(int temperaturaCongelacion) {
		this.temperaturaCongelacion = temperaturaCongelacion;
	}
	
	 @Override
	    public String toString() {
	        return "Temperatura de congelación recomendada:  " + temperaturaCongelacion;
	    }

}
